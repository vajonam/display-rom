#!/system/bin/sh

su -c 'sh /system/xbin/busybox-links.sh'

export SETTINGS_FILE="/maps/settings.txt"
export INTERNAL="/cache/settings.sh"

SETTINGS=$'
setprop persist.sys.map.scheme.tracking "carnav.traffic.night"
setprop persist.sys.map.scheme.enroute "carnav.traffic.night"
setprop persist.sys.hud_gps 0 # -1 = switch between phone and ublox allowed
setprop persist.sys.map.update 0 # enable maps update menu item
setprop persist.sys.extruded_buildings 0 # here maps settings
setprop persist.sys.landmarks_visible 0 # here maps settings
setprop persist.sys.speedlimit.always 0 # speed limit always visible on maps
setprop persist.sys.music_glance_fade_black 1 # music glance artwork fade from black
#setprop persist.sys.override_locale "zh-Hant-HK" # BCP 47 Language Tag, disabled by default, remove the first # to apply. Tag e.g. "fr-CA", "es-419", "de-DE" (https://www.w3.org/International/articles/language-tags/)
'

if [[ ! -f "${SETTINGS_FILE}" ]]; then
  touch "${SETTINGS_FILE}"
fi

dos2unix < "${SETTINGS_FILE}" > "${INTERNAL}"

# Iterate over each line in the settings block above
while read -r line ; do
  if [[ ! -z "$line" ]]; then

    # Check if settings line (first two fields) exists in settings file.
    if ! grep "$(echo "${line#"${line%%[!#]*}"}" | cut -d' ' -f1-2)" "${INTERNAL}"; then

      # If not, add it
      echo "Adding $line to ${INTERNAL}"
      echo "" >> "${INTERNAL}"
      echo "$line" >> "${INTERNAL}"
    fi
  fi
done <<< "$SETTINGS"

cat "${INTERNAL}"  | tr -s '\n' | unix2dos > "${SETTINGS_FILE}"

su -c "/system/bin/sh \"${INTERNAL}\""

